
#include <linux/module.h>	//	normal includes

#include <linux/fs.h>		//	file_operations
#include <linux/cdev.h>		//	cdev_init, cdev_add, cdev_del

#include <linux/kdev_t.h>	//	MOJOR, MINOR

#include <linux/device.h>


//	self include files.
struct cd_data_t{
	struct cdev cd_cdev;

	struct class *cd_class;
	struct class *cd_class1;

	int major;
	char *dev_name;
	int dev_count;

	atomic_t open_count;
	int max_user;
	spinlock_t spinlock;

	struct mutex cd_mutex;
	struct semaphore cd_sema;

	int hd_heavy_level;

	//	... more customer informations
	
};

static struct cd_data_t cd_data = {
	.major = 248,
	.dev_name = "cd_sample",
	.dev_count = 2,

	.open_count = ATOMIC_INIT(0),
	.max_user = 3,


	.hd_heavy_level = 0,
	
};

static int cd_open(struct inode *nodep, struct file *filp)
{
	struct cdev *p_cdev = nodep->i_cdev;
	struct cd_data_t *p_cd_data = container_of(p_cdev, struct cd_data_t, cd_cdev);

	int ret = 0;
	unsigned long flags;

	printk(KERN_ERR "Enter %s!\n", __func__);
	
	filp->private_data = (void *)p_cd_data;


	if(0 == p_cd_data->hd_heavy_level){
		//	light hardware & resource operations
		spin_lock_irqsave(&p_cd_data->spinlock, flags);
		if(atomic_inc_return(&p_cd_data->open_count) >= p_cd_data->max_user){
			atomic_dec(&p_cd_data->open_count);
			spin_unlock_irqrestore(&p_cd_data->spinlock, flags);
			printk(KERN_ERR "Too many user!\n");
			ret = -EBUSY;
			goto busy;
		}
		spin_unlock_irqrestore(&p_cd_data->spinlock, flags);
		
	} else if(1 == p_cd_data->hd_heavy_level){
	
		//	heavy hardware & resource operations
		mutex_lock(&p_cd_data->cd_mutex);

		if(atomic_inc_return(&p_cd_data->open_count) >= p_cd_data->max_user){
		
			atomic_dec(&p_cd_data->open_count);
			mutex_unlock(&p_cd_data->cd_mutex);
			printk(KERN_ERR "Too many user!\n");
			ret = -EBUSY;
			goto busy;
			
		}
		
		if(atomic_read(&p_cd_data->open_count) == 1){
			//	this is the first open, do something special about hardware, if need!
			//	..
			
		}

		//	do something about hardware 
		//	...
		
		mutex_unlock(&p_cd_data->cd_mutex);
	} else if(2 == p_cd_data->hd_heavy_level){

		if(down_trylock(&p_cd_data->cd_sema) != 0){
			ret = -EBUSY;
			goto busy;
		}
		
	}

	return 0;
	
busy:
	
	return ret;
	
}

static int cd_release(struct inode *nodep, struct file *filp)
{
	struct cd_data_t *p_cd_data = (struct cd_data_t *)filp->private_data;
	unsigned long flags;

	printk(KERN_ERR "Enter %s!\n", __func__);

	if(0 == p_cd_data->hd_heavy_level){

		spin_lock_irqsave(&p_cd_data->spinlock, flags);
		atomic_dec(&p_cd_data->open_count);
		spin_unlock_irqrestore(&p_cd_data->spinlock, flags);

	} else if(1 == p_cd_data->hd_heavy_level){
	
		mutex_lock(&p_cd_data->cd_mutex);

		//	do something about hardware 
		//	...
		
		if(atomic_dec_and_test(&p_cd_data->open_count)){
			//	this is the last close, do something special about hardware, if need!
			//	..
		}
		
		mutex_unlock(&p_cd_data->cd_mutex);

	} else if(2 == p_cd_data->hd_heavy_level){
		
		up(&p_cd_data->cd_sema);
	}

	return 0;
}

static ssize_t cd_read(struct file *filp, char __user *buf, size_t size, loff_t *off)
{
	struct cd_data_t *p_cd_data = (struct cd_data_t *)filp->private_data;

	printk("Enter %s %s\n", __func__, p_cd_data->dev_name);

	

	
	
	return 0;
}




static ssize_t cd_write(struct file *filp, const char __user *buf, size_t size, loff_t *off)
{
	struct cd_data_t *p_cd_data = (struct cd_data_t *)filp->private_data;

	printk("Enter %s %s, size = %d\n", __func__, p_cd_data->dev_name, size);


	return size;

	
}




static const struct file_operations cd_fps = 
{
	.owner = THIS_MODULE,

	.open = cd_open,
	.release = cd_release,

	.read = cd_read,
	.write = cd_write,
	
};


static void chardev_sample_exit(void)
{
	int minor = 0;
	
	device_destroy(cd_data.cd_class, MKDEV(cd_data.major, minor));
	device_destroy(cd_data.cd_class1, MKDEV(cd_data.major, minor+1));
	
	class_destroy(cd_data.cd_class);
	class_destroy(cd_data.cd_class1);

	cdev_del(&(cd_data.cd_cdev));
	
	unregister_chrdev_region(MKDEV(cd_data.major, minor), cd_data.dev_count);

	return;
}


static int chardev_sample_init(void)
{
	int ret;
	dev_t devno;
	int minor;

	if(cd_data.major > 0){
		devno = MKDEV(cd_data.major, 0);
		ret = register_chrdev_region(devno, cd_data.dev_count, "chardev_sample");
		if(ret < 0){
			printk(KERN_ERR "Failed to register chrdev region, major = %d\n", cd_data.major);
			return ret;
		}
	} else {
		//	dynamically alloc char dev region
		ret = alloc_chrdev_region(&devno, 0, cd_data.dev_count, "chardev_sample");
		if(ret < 0){
			printk(KERN_ERR "Failed to alloc char device number, ret = %d\n", ret);
			return ret;
		}
		cd_data.major = MAJOR(devno);
	}

	//	init, set cd_fps to cd_cdev.
	cdev_init(&(cd_data.cd_cdev), &cd_fps);
	cd_data.cd_cdev.owner = THIS_MODULE;

	ret = cdev_add(&(cd_data.cd_cdev), devno, cd_data.dev_count);
	if(ret < 0){
		printk(KERN_ERR "Failed to add cdev! ret = %d\n", ret);
		goto unreg_chrdev_region;
	}

	minor = MINOR(devno);
	
	cd_data.cd_class = class_create(THIS_MODULE, "cd_sample0");
	device_create(cd_data.cd_class, NULL, MKDEV(cd_data.major, minor), 
			(void *)&(cd_data.cd_cdev), "cd_sample%d", 0);

	cd_data.cd_class1 = class_create(THIS_MODULE, "cd_sample1");
	device_create(cd_data.cd_class1, NULL, MKDEV(cd_data.major, minor+1), 
			(void *)&(cd_data.cd_cdev), "cd_sample%d", 1);

	spin_lock_init(&cd_data.spinlock);
	mutex_init(&cd_data.cd_mutex);
	sema_init(&cd_data.cd_sema, 3);
	
	printk(KERN_ERR "Created %d devices, major = %d, start minor = %d\n", 
			cd_data.dev_count, cd_data.major, minor);

	return 0;

unreg_chrdev_region:
	unregister_chrdev_region(devno, cd_data.dev_count);
	
	
	return ret;
	
}

module_init(chardev_sample_init);
module_exit(chardev_sample_exit);

MODULE_LICENSE("GPL");


