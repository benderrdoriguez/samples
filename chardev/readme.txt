
一个字符设备驱动的流程

一、编写一个Linux char device driver，第一步是安排好 dev_t ，即设备号。

	设备号分成两部分，major & minor， major & minor 和 dev_t 。
	它们之间的转换有专门的函数完成，可以确保良好的扩展性，包括:

	dev_t = mkdev(major, minor)
	major = MAJOR(dev_t)
	minor = MINOR(dev_t)

	其中，有一部分 major 在系统中是已经被占用了的，不能被我们使用。
	
	设备号的设定有两种方式，一种是固定设备号、还有一种是动态申请。

	方法一、固定设备号需要确保该major没有被使用，方式如下：

	major = fix_major_number, minor = dev_count
	dev_t devno = mkdev(major, minor)
	register_chrdev_region(devno, minor, "dev_name"); 
	需要判断返回值，返回0表示成功。
	申请的设备号需要释放:
	unregister_chrdev_region(devno, dev_count);

	方法二、动态申请设备号的方式如下:
	dev_t devno;
	ret = alloc_chrdev_region(&devno, baseminor, dev_count, "dev_name);
	返回负值表示失败.
	申请的设备号需要释放:
	unregister_chrdev_region(devno, dev_count);


二、有了可以使用的设备号之后，就可以创建、添加char设备了，有 x 
	中方式实现这一功能。

	方法一、cdev:
	分成两步:
	static struct cdev mycdev;
	static const struct file_operations dev_fps = { ... };
	cdev_init(&mycdev, &dev_fps);
	cdev_add(&mycdev, devno, dev_count);

	cdev_init完成对 mycdev 的基本初始化，并且将设备操作函数结构体 dev_fps 
	绑定到 mycdev 中。
	cdev_add 完成向系统添加设备。
	在移除该设备的时候，则比较简单:
	cdev_del(&mycdev);
	
	方法二、

	

三、关于设备节点

	对于用户空间的程序来说，接口是 /dev/ 
	目录下的设备节点，设备节点的创建也有不少方法:

	方法一、手动创建，或者说启动脚本创建
	在获知设备的major 和 minor 的情况下，可以用命令 mknod 手动创建设备节点。
	或者将命令添加到某一个启动脚本中完成设备节点的创建。

	这种方法的前提是必须知道设备的 major 和 minor; 不需要时，要手动删除

	方法二、自动创建节点
	p_dev_class = class_create(owner, "dev_name");
	device_create(p_dev_class, dev_parent, devno, dev_fps, fmt...);
	删除节点，可以在驱动中添加:
	device_destory(p_dev_class, devno);
	class_destory(p_dev_class);

	方法三、udev
	不过好像方法二在没有udev的情况下也能添加设备节点了? 那 udev 还有什么用呢?


	
四、






