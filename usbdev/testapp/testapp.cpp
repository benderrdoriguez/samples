

#include <stdio.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>

#include <string.h>

#include <errno.h>

typedef struct _USBCB		/* USB command block */
{
  unsigned int u32_Command;		/* command to execute */
  unsigned int u32_Data;		/* generic data field */
  unsigned int u32_Count;		/* number of bytes to transfer */
} USBCB, *PUSBCB;


enum _VERSION_STRINGS		/* version string info */
{
	FW_BUILD_DATE,			/* build date of firmware */
	FW_BUILD_TIME,			/* build time of firmware */
	FW_VERSION_NUMBER,		/* version number of firmware */
	FW_TARGET_PROC,			/* target processor of firmware */
	FW_APPLICATION_NAME,	/* application name of firmware */

	NUM_VERSION_STRINGS		/* number of version strings */
};


#define	MAX_VERSION_STRING_LEN		32


#define 	ERR		printf
#define		DBG		ERR

static void display(unsigned char *pbyVersion)
{
	// we are all set to display the strings
	printf("\nFirmware version: %s \n", (char*)(pbyVersion + (FW_VERSION_NUMBER*MAX_VERSION_STRING_LEN)) );
	
	printf("\nBuild date:       %s \n", (char*)(pbyVersion + (FW_BUILD_DATE*MAX_VERSION_STRING_LEN)) );
	printf("\nBuild time:       %s \n", (char*)(pbyVersion + (FW_BUILD_TIME*MAX_VERSION_STRING_LEN)) );
	printf("\nTarget processor: %s \n", (char*)(pbyVersion + (FW_TARGET_PROC*MAX_VERSION_STRING_LEN)) );
	printf("\nApplication name: %s \n", (char*)(pbyVersion + (FW_APPLICATION_NAME*MAX_VERSION_STRING_LEN)) );
}


static int read_dev(int fd, char *buf, size_t max_size, unsigned int timeout_ms = 0)
{
	int ret = 0;
	struct timeval tv;
	int max_fd = fd;
	fd_set fds;

	if(fd < 0){
		ERR("Open device first!\n");
		return -1;
	}

	if(0 == timeout_ms){
		goto dread;
	}

	tv.tv_sec = timeout_ms / 1000;
	tv.tv_usec = (timeout_ms % 1000) * 1000;

	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	ret = select(fd+1, &fds, NULL, NULL, 0);
	if(0 == ret){
		DBG("Select time out!\n");
		return 0;
	}

	if(ret < 0){
		ERR("Device error happend: %d\n", errno);
		return -errno;
	}

dread:	
	return read(fd, buf, max_size);
	
}


int main(int argc, char **argv)
{
	char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', ' ', '\r', '\n'};
	int fd;
	USBCB cmd;
	unsigned char buf[1024];

	int ffd;
	
	
	printf("Enter !\n");

	fd = open("/dev/transcom0", O_RDWR);
	if(fd < 0){
		perror("open!\n");
		return -1;
	}
	printf("Open succ!\n");

#if 1
	sleep(1);

	cmd.u32_Command = 1;
	cmd.u32_Count = 160;
	cmd.u32_Data = 0;

	int ret = 0;
	ret = write(fd, &cmd, sizeof(USBCB));
	printf("write %d bytes\n", ret);
	if(ret < 0){
		goto out;
	}

#if 0
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
	write(fd, &cmd, sizeof(USBCB));
#endif

//	sleep(1);

	memset(buf, 0x0, sizeof(buf));
//	ret = read(fd, buf, sizeof(buf));

	ret = read_dev(fd, (char *)buf, sizeof(buf), 1000);
	

	if(ret > 0){
		ffd = open("version.txt", O_RDWR | O_CREAT);

	#if 0
		for(int i = 0; i < ret; ++i){
			write(ffd, &hex[buf[i]&0xF0 >> 8], 1);
			write(ffd, &hex[buf[i]&0x0F], 1);
			write(ffd, &hex[0x10], 1);

			if((i+1) % 8 == 0){
				write(ffd, &hex[0x11], 1);
				write(ffd, &hex[0x12], 1);
			}
		}
	#endif
		write(ffd, buf, ret);
	
		close(ffd);
	}

	printf("get %d bytes: %s!\n", ret, buf);

//	display(buf);
#endif


out:
	close(fd);
	

	return 0;
}



