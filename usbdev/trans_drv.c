

#include <linux/module.h>	//	normal includes

#include <linux/slab.h>


#include <linux/fs.h>		//	file_operations
#include <linux/cdev.h>		//	cdev_init, cdev_add, cdev_del

#include <linux/kdev_t.h>	//	MOJOR, MINOR

#include <linux/device.h>

#include <linux/usb.h>

#include <asm/uaccess.h>		//	copy_from_user

#define		TRANS_DBG		0

#define		LIMIT_RDWR		0

#define		IN_BUF_COUNT		8
#define		OUT_BUF_COUNT		4

#define		SUBMIT_OPEN		0

#define		ADD_CHAR_INIT		1
#define		AUTO_CREATE_DEV	0

//	self include files.
struct trans_data_t{
	struct cdev trans_cdev;
	struct class *trans_class;

	int major;
	int minor;
	char *dev_name;
	int dev_count;

	int present;

	struct mutex openmutex;
	int max_user;
	int open_count;

#if	LIMIT_RDWR
	struct mutex rdwrmutex;
#endif

	u8 *saved_buf;
	size_t saved_buf_size;
	size_t saved_write, saved_read;

	u8 *in_buf[IN_BUF_COUNT];
	u8 *out_buf[OUT_BUF_COUNT];
	
	struct urb *in_urb[IN_BUF_COUNT];
	
	struct urb *out_urb[OUT_BUF_COUNT];
	unsigned long out_busy;
	unsigned long tx_time[OUT_BUF_COUNT];

	size_t buf_size;


	//	for debug
	int alloc_count;
	int free_count;

	
};

static struct trans_data_t trans_data = {
#if AUTO_CREATE_DEV
	.major = -1,
	.minor = -1,
#else
	.major = 249,
	.minor = 0,
#endif
	.dev_name = "transcom",

	.dev_count = 1,

	.present = 0,

	.max_user = 8,
	.open_count = 0,

	.saved_buf = NULL,
	.saved_write = 0,
	.saved_read = 0,

	.out_busy = 0,

	.buf_size = 1024,

	.alloc_count = 0,
	.free_count = 0,
};




static int trans_open(struct inode *nodep, struct file *filp)
{
	struct cdev *p_cdev = nodep->i_cdev;
	struct trans_data_t *p_data = container_of(p_cdev, struct trans_data_t, trans_cdev);
	int ret = 0;

#if TRANS_DBG
	printk(KERN_DEBUG "[yithe]Enter %s!\n", __func__);
#endif

	if(p_data->present <= 0){
		printk(KERN_DEBUG "[yithe]Connect dev first!\n");
		return -EIO;
	}
	
	filp->private_data = (void *)p_data;

	mutex_lock(&p_data->openmutex);
	if(++p_data->open_count > p_data->max_user){
		printk(KERN_ERR "[yithe]Device is busy, only allow %d user max\n", p_data->max_user);
		p_data->open_count--;
		ret = -EIO;
		mutex_unlock(&p_data->openmutex);
		return ret;
	}
	mutex_unlock(&p_data->openmutex);

#if SUBMIT_OPEN
{
	int idx;
	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		ret = usb_submit_urb(p_data->in_urb[idx], GFP_ATOMIC);
		if (ret) {
			printk(KERN_ERR "[yithe] Prepare for in urb failed! ret = %d\n", ret);
			return -EIO;
		}
	}
}
#endif

	return ret;
	
}

static int trans_release(struct inode *nodep, struct file *filp)
{
	struct trans_data_t *p_data = (struct trans_data_t *)filp->private_data;

#if TRANS_DBG
	printk(KERN_DEBUG "[yithe]Enter %s!\n", __func__);
#endif

#if SUBMIT_OPEN
{
	int idx;
	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		usb_kill_urb(p_data->in_urb[idx]);
	}
}
#endif

	mutex_lock(&p_data->openmutex);
	
	if(p_data->open_count > 0){
		p_data->open_count--;
	} else {
		printk(KERN_ERR "[yithe]p_data->open_count = %d\n", p_data->open_count);
	}
	
	mutex_unlock(&p_data->openmutex);
	
	return 0;
	
}

static ssize_t trans_read(struct file *filp, char __user *buf, size_t size, loff_t *off)
{
	size_t write, read, len;
	size_t todo;
	struct trans_data_t *p_data = (struct trans_data_t *)filp->private_data;

#if LIMIT_RDWR
	mutex_lock(&p_data->rdwrmutex);
#endif	

#if TRANS_DBG
	printk(KERN_DEBUG "[yithe]Enter %s %s\n", __func__, p_data->dev_name);
#endif

	if(p_data->present <= 0){
		printk(KERN_DEBUG "[yithe]Connect dev first!\n");
		return -EIO;
	}
	
	write = p_data->saved_write;
	read = p_data->saved_read;
	len = p_data->saved_buf_size;

	if(write == read){
	#if LIMIT_RDWR
		mutex_unlock(&p_data->rdwrmutex);
	#endif
		return 0;
	}

	if(write > read){
		todo = write - read;
	} else {
		todo = len - read + write;
	}

	if(todo > size){
		todo = size;
	}

	if(todo + read <= len){
		if(copy_to_user(buf, &p_data->saved_buf[read], todo)){
			printk(KERN_ERR "[yithe]Copy error!\n");
			goto err;
		}
	} else {
		if(copy_to_user(buf, &p_data->saved_buf[read], len - read)){
			printk(KERN_ERR "[yithe]Copy error!\n");
			goto err;
		}

		if(copy_to_user(&buf[len - read], &p_data->saved_buf[0], todo + read - len)){
			printk(KERN_ERR "[yithe]Copy error!\n");
			goto err;
		}
	}


	read += todo;
	if(read >= len){
		read -= len;
	}
	
	p_data->saved_read = read;

#if LIMIT_RDWR
	mutex_unlock(&p_data->rdwrmutex);
#endif

	return todo;

err:
#if LIMIT_RDWR
	mutex_unlock(&p_data->rdwrmutex);
#endif
	return -EIO;
	
}




static ssize_t trans_write(struct file *filp, const char __user *buf, size_t size, loff_t *off)
{
	int idx, urb_idx, err;
	size_t todo;
	struct urb *out_urb = NULL;
	struct trans_data_t *p_data = (struct trans_data_t *)filp->private_data;

#if LIMIT_RDWR
	mutex_lock(&p_data->rdwrmutex);
#endif

#if TRANS_DBG
	printk(KERN_DEBUG "[yithe]Enter %s %s, size = %d\n", __func__, p_data->dev_name, size);
#endif

	if(p_data->present <= 0){
		printk(KERN_DEBUG "[yithe]Connect dev first!\n");
		return -EIO;
	}

	todo = size;
	if(todo > p_data->buf_size){
		printk(KERN_ERR "[yithe] Size(%d) is too large, should less than %d\n", size, p_data->buf_size);
		todo = p_data->buf_size;
	}

	urb_idx = -1;
	smp_mb__before_clear_bit();
	for(idx = 0; idx < OUT_BUF_COUNT && urb_idx < 0; ++idx){
		out_urb = p_data->out_urb[idx];
		if(test_and_set_bit(idx, &p_data->out_busy)) {

			if (!time_before(jiffies, p_data->tx_time[idx] + 10 * HZ)){
				printk(KERN_ERR "Warning, unlink a timeout write urb!\n");
				usb_unlink_urb(out_urb);
			}
			
		} else {
			urb_idx = idx;
			
			break;
		}
	}

	if(urb_idx < 0){
		printk(KERN_ERR "Warning, all urbs are busy!\n");
	#if LIMIT_RDWR
		mutex_unlock(&p_data->rdwrmutex);
	#endif
		return -EBUSY;
	}

	if(copy_from_user(out_urb->transfer_buffer, buf, todo)){
		printk(KERN_ERR "[yithe] Copy from user error!\n");
		#if LIMIT_RDWR
			mutex_unlock(&p_data->rdwrmutex);
		#endif
		smp_mb__before_clear_bit();
		clear_bit(urb_idx, &p_data->out_busy);
		return -EIO;
	}
	out_urb->transfer_buffer_length = todo;

	err = usb_submit_urb(out_urb, GFP_ATOMIC);
	if(err){
		printk(KERN_ERR "[yithe] Submit write urb failed. err = %d\n", err);
	#if LIMIT_RDWR
		mutex_unlock(&p_data->rdwrmutex);
	#endif
		smp_mb__before_clear_bit();
		clear_bit(urb_idx, &p_data->out_busy);
		return -EIO;
	}

	p_data->tx_time[urb_idx] = jiffies;

#if LIMIT_RDWR
	mutex_unlock(&p_data->rdwrmutex);
#endif

	return todo;
	
}




static const struct file_operations trans_fps = 
{
	.owner = THIS_MODULE,

	.open = trans_open,
	.release = trans_release,

	.read = trans_read,
	.write = trans_write,

};


static struct usb_device_id transcom_usb_table[] = {

	{USB_DEVICE(0x064B, 0x1212)},
	
	{ }
};

static void transcom_indata_callback(struct urb *urb)
{
	u32 len, write, todo;
	unsigned char *data = urb->transfer_buffer;
	u32	actual_length = urb->actual_length;
	int status = urb->status;
	struct trans_data_t *p_data = (struct trans_data_t *)urb->context;

#if TRANS_DBG
	printk(KERN_DEBUG "[yithe]Enter in callback! actual_length=%d, status = %d\n", actual_length, status);
#endif

	if(p_data->present == 0){
		goto quit;
	} else if(status){
	#if TRANS_DBG
		printk(KERN_DEBUG "[yithe]Indata, p_data->present = %d, status = %d\n", p_data->present, status);
	#endif
		goto out;
	}

	if(actual_length) {
		len = p_data->saved_buf_size;
		write = p_data->saved_write;

		todo = actual_length;
		if(todo + write > len){
			memcpy(&p_data->saved_buf[write], data, len - write);
			memcpy(&p_data->saved_buf[0], &data[len - write], actual_length + write - len);
		} else {
			memcpy(&p_data->saved_buf[write], data, todo);
		}

		write += actual_length;
		if(write > len){
			write -= len;
		}

		p_data->saved_write = write;
	}

out:
	if (status != -ESHUTDOWN && status != -ENOENT && status != -ECONNRESET) {
		int err = 0;

		if(status){
		//	printk(KERN_DEBUG "[yithe]Enter in callback! Submit urb but status is %d\n", status);
		} else {
			err = usb_submit_urb(urb, GFP_ATOMIC);
			if (err && err != -EPERM) {
				printk(KERN_ERR "[yithe]resubmit read urb failed. "
					"(%d) status=%d", err, status);
			}
		}
	}
	
quit:
	return;
	
}

static void transcom_outdata_callback(struct urb *urb)
{
	int idx;
	struct trans_data_t *p_data = (struct trans_data_t *)urb->context;
	
#if TRANS_DBG
	u32	actual_length = urb->actual_length;
	
	printk(KERN_DEBUG "[yithe]Enter out callback! %p, size = %d\n", urb, actual_length);
#endif

	for(idx = 0; idx < OUT_BUF_COUNT; ++idx){
		if(p_data->out_urb[idx] == urb){
			smp_mb__before_clear_bit();
			clear_bit(idx, &p_data->out_busy);
		}
	}
	
}




static void remove_char_dev(struct trans_data_t *p_data)
{
#if AUTO_CREATE_DEV
	device_destroy(p_data->trans_class, MKDEV(p_data->major, p_data->minor));
	class_destroy(p_data->trans_class);
#endif
	cdev_del(&(p_data->trans_cdev));
	unregister_chrdev_region(MKDEV(p_data->major, p_data->minor), p_data->dev_count);
}

static int add_char_dev(struct trans_data_t *p_data)
{
	int ret;
	dev_t devno;
	
	if(p_data->major > 0){
		devno = MKDEV(p_data->major, 0);
		ret = register_chrdev_region(devno, p_data->dev_count, p_data->dev_name);
		if(ret < 0){
			printk(KERN_ERR "[yithe]Failed to register chrdev region, major = %d\n", p_data->major);
			return -EFAULT;
		}
	} else {
		//	dynamically alloc char dev region
		ret = alloc_chrdev_region(&devno, 0, p_data->dev_count, p_data->dev_name);
		if(ret < 0){
			printk(KERN_ERR "[yithe]Failed to alloc char device number, ret = %d\n", ret);
			return -EFAULT;
		}
		p_data->major = MAJOR(devno);
	}

	//	init, set trans_fps to trans_cdev.
	cdev_init(&(p_data->trans_cdev), &trans_fps);
	p_data->trans_cdev.owner = THIS_MODULE;

	ret = cdev_add(&(p_data->trans_cdev), devno, p_data->dev_count);
	if(ret < 0){
		printk(KERN_ERR "[yithe]Failed to add cdev! ret = %d\n", ret);
		goto unreg_chrdev_region;
	}

	p_data->minor = MINOR(devno);
	
#if AUTO_CREATE_DEV
	p_data->trans_class = class_create(THIS_MODULE, p_data->dev_name);
	device_create(p_data->trans_class, NULL, devno, 
			(void *)&(p_data->trans_cdev), "%s%d", p_data->dev_name, 0);
#endif

	return 0;
	
unreg_chrdev_region:
	unregister_chrdev_region(devno, p_data->dev_count);

	return ret;
	
}


static int transcom_usb_suspend(struct usb_interface *intf, pm_message_t message)
{
	printk(KERN_INFO "[yithe]Enter suspend!\n");
	
	return -EBUSY;
}


static int transcom_usb_resume(struct usb_interface *intf)
{

	printk(KERN_INFO "[yithe]Enter resume!\n");
	
	return 0;
}


static void transcom_usb_exit(struct usb_interface *intf)
{
	
	
	struct trans_data_t *p_data = &trans_data;
	
	printk(KERN_INFO "[yithe]Enter exit!\n");

	p_data->present = 0;
#if SUBMIT_OPEN

#else
	{
		int idx;
		for(idx = 0; idx < IN_BUF_COUNT; ++idx){
			//	Need modify!
	//		usb_kill_urb(p_data->in_urb[idx]);
		}
	}
#endif	

#if ADD_CHAR_INIT
#else
	remove_char_dev(p_data);
#endif

}


static int transcom_usb_probe(struct usb_interface *intf,
			    const struct usb_device_id *id)
{
	int ret = 0;
	int idx;
	struct usb_device *dev = interface_to_usbdev(intf);
	unsigned int inpipe = usb_rcvbulkpipe(dev, intf->cur_altsetting->endpoint[0].desc.bEndpointAddress);
	unsigned int outpipe = usb_sndbulkpipe(dev, intf->cur_altsetting->endpoint[1].desc.bEndpointAddress);
	struct trans_data_t *p_data = &trans_data;
	
	printk(KERN_INFO "[yithe]Enter probe!\n");
		
	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		usb_fill_bulk_urb(p_data->in_urb[idx], dev, inpipe, (void *)p_data->in_buf[idx],
		 	p_data->buf_size, transcom_indata_callback, (void *)p_data);
	}

	for(idx = 0; idx < OUT_BUF_COUNT; ++idx){
		usb_fill_bulk_urb(p_data->out_urb[idx], dev, outpipe, (void *)p_data->out_buf[idx],
		 	p_data->buf_size, transcom_outdata_callback, (void *)p_data);
	}

#if ADD_CHAR_INIT
#else
	ret = add_char_dev(p_data);
	if(ret < 0){
		return ret;
	}
#endif
	printk(KERN_INFO "[yithe]Created %d devices, major = %d, start minor = %d\n", 
			p_data->dev_count, p_data->major, p_data->minor);

#if SUBMIT_OPEN

#else
	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		ret = usb_submit_urb(p_data->in_urb[idx], GFP_ATOMIC);
		if (ret) {
			printk(KERN_ERR "[yithe] In probe, prepare for in urb failed! ret = %d\n", ret);
			remove_char_dev(p_data);
			return ret;
		}
	}
#endif

	p_data->present = 1;

	return ret;
	
}



static struct usb_driver trans_usb_drv = {
	.name = "transcomusb",
	.probe = transcom_usb_probe,
	.disconnect = transcom_usb_exit,

	.suspend = transcom_usb_suspend,
	.resume = transcom_usb_resume,

	.supports_autosuspend = 0,
	
	.id_table = transcom_usb_table,
};



static void free_res(struct trans_data_t *p_data)
{
	int idx;
	
	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		if(p_data->in_urb[idx]){
			usb_free_urb(p_data->in_urb[idx]);
			p_data->in_urb[idx] = NULL;

			p_data->free_count++;
		}
		if(p_data->in_buf[idx]){
			kfree(p_data->in_buf[idx]);
			p_data->in_buf[idx] = NULL;

			p_data->free_count++;
		}
	}

	for(idx = 0; idx < OUT_BUF_COUNT; ++idx){
		if(p_data->out_urb[idx]){
			usb_free_urb(p_data->out_urb[idx]);
			p_data->out_urb[idx] = NULL;

			p_data->free_count++;
		}
		
		if(p_data->out_buf[idx]){
			kfree(p_data->out_buf[idx]);
			p_data->out_buf[idx] = NULL;

			p_data->free_count++;
		}
	}

	if(p_data->saved_buf){
		kfree(p_data->saved_buf);
		p_data->saved_buf = NULL;
		p_data->saved_write = p_data->saved_read = 0;

		p_data->free_count++;
	}
	
}


static int alloc_res(struct trans_data_t *p_data)
{
	int ret = 0;
	int idx;
	
	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		p_data->in_buf[idx] = (u8 *)(kmalloc(p_data->buf_size, GFP_KERNEL));

		if(p_data->in_buf[idx] == NULL){
 			printk(KERN_ERR "[yithe] Not enough memory for in %d!\n", idx);
 			ret = -ENOMEM;
			goto err;
		}

		p_data->alloc_count++;

		memset(p_data->in_buf[idx], idx, p_data->buf_size);

		p_data->in_urb[idx] = usb_alloc_urb(0, GFP_KERNEL); 
		if(p_data->in_urb[idx] == NULL){
			printk(KERN_ERR "[yithe] Not enough memory for in urb %d!\n", idx);
			ret = -ENOMEM;
			goto err;
		}

		p_data->alloc_count++;
		
	}


	for(idx = 0; idx < OUT_BUF_COUNT; ++idx){
		p_data->out_buf[idx] = (u8 *)(kmalloc(p_data->buf_size, GFP_KERNEL));

		if(p_data->out_buf[idx] == NULL){
			printk(KERN_ERR "[yithe] Not enough memory for out!\n");
			ret = -ENOMEM;
			goto err;
		}

		p_data->alloc_count++;
		
		p_data->out_urb[idx] = usb_alloc_urb(0, GFP_KERNEL); 
		if(p_data->out_urb[idx] == NULL){
			printk(KERN_ERR "[yithe] Not enough memory for out urb!\n");
			ret = -ENOMEM;
			goto err;
		}

		p_data->alloc_count++;

	}

	p_data->saved_buf_size = p_data->buf_size*IN_BUF_COUNT;
	p_data->saved_buf = (u8 *)(kmalloc(p_data->saved_buf_size, GFP_KERNEL));

	if(p_data->saved_buf == NULL){
		printk(KERN_ERR "[yithe] Not enough memory for saved buf!\n");
		ret = -ENOMEM;
		goto err;
	}

	p_data->alloc_count++;


	return 0;
err:
	free_res(p_data);

	return ret;
	
}



static void transcom_exit(void)
{
	struct trans_data_t *p_data = &trans_data;
	
	printk(KERN_INFO "[yithe]Module exit!\n");

#if ADD_CHAR_INIT
	remove_char_dev(p_data);
#endif

	usb_deregister(&trans_usb_drv);
	
	free_res(p_data);

	if(p_data->alloc_count != p_data->free_count){
		printk(KERN_ERR "[yithe] alloc %d free %d\n", p_data->alloc_count, p_data->free_count);
	}

	return;
}

static int transcom_init(void)
{
	int ret;
	int idx;
	struct trans_data_t *p_data = &trans_data;

	printk(KERN_INFO "[yithe]Module init!\n");

	for(idx = 0; idx < IN_BUF_COUNT; ++idx){
		p_data->in_buf[idx] = NULL;
		p_data->in_urb[idx] = NULL;
	}

	if(alloc_res(p_data)){
		return -ENOMEM;
	}
	
	if ((ret = usb_register(&trans_usb_drv))) {
		printk(KERN_ERR "[yithe]usb_register failed. (%d)", ret);
		goto err;
	}

	
#if ADD_CHAR_INIT
	ret = add_char_dev(p_data);
	if(ret < 0){
		usb_deregister(&trans_usb_drv);
		goto err;
	}
#endif

	mutex_init(&p_data->openmutex);

#if LIMIT_RDWR
	mutex_init(&p_data->rdwrmutex);
#endif

	return 0;	

err:
	free_res(p_data);
	return ret;
	
}

module_init(transcom_init);
module_exit(transcom_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("172552062@qq.com");

